<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/about', 'PagesController@about');

Route::get('/menu', 'PagesController@menu');

Route::get('/contact', 'PagesController@contact');

Route::get('/product', 'PagesController@product');

Route::get('/menulist', 'PagesController@menulist');

Route::get('/libations', 'PagesController@libations');

Route::get('/appetizers', 'PagesController@appetizers');

Route::get('/third', 'PagesController@third');

Route::get('/bar', 'PagesController@bar');

Route::get('/fourth', 'PagesController@fourth');

Route::get('/fifth', 'PagesController@fifth');

Route::get('/sixth', 'PagesController@sixth');

Route::get('/seventh', 'PagesController@seventh');

Route::get('/eighth', 'PagesController@eighth');

Route::get('/ninth', 'PagesController@ninth');

Route::get('/tenth', 'PagesController@tenth');
