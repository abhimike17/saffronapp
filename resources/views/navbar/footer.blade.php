</br>
<footer>
    <div class="saffron_container">
        <div class="row">
            <div class="col col-12">

                {{--<div class="saffron_foter_text">+1 215 456 15 15. <span>8:00 am – 11:30 pm</span></div>--}}
                <ul class="saffron_foter_menu">
                    <li><a href="/">Home</a></li>
                    <li><a href="/menu">Menu</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
                <ul class="saffron_social">
                    <li><a href="https://twitter.com/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.instagram.com/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
                <div class="saffron_copy_text">Copyright © 2018 saffron. All Rights Reserved.</div>
            </div>
        </div>
    </div>
</footer>
