<body class="saffron_home6 saffron_home1">
<header class="saffron_header">
    <div class="row ">
        <div class="col col-3 saffron_header_left">
            <div class="saffron_inner">
                {{--<div class="saffron_inner_h_contact">--}}
                    {{--<div class="saffron_h_phone">+1 215 456 15 15</div>--}}
                    {{--<div class="saffron_h_wh">8:00 am – 11:30 pm</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col col-6 saffron_header_center">
            <div class="saffron_def_header">
                <div class="saffron_logo_cont">
                    <a href="/" class="saffron_image_logo">
                    </a>
                </div>
                <nav class="saffron_menu_cont">
                    <ul id="menu-main-menu" class="saffron_menu">
                        <li class=" menu-item "><a href="/">Home</a>
                        <li class=" menu-item menu-item-has-children"><a href="#">Menu</a>
                            <ul class="sub-menu">
                                <li class="menu-item "><a href="/menu">Lunch Menu</a></li>
                                <li class="menu-item"><a href="/menulist">Dinner Menu</a></li>
                            </ul>
                        </li>
                        <li class="menu-item "><a href="/about">About</a>

                        </li>
                        <li class="menu-item "><a href="/bar">Libations & Bar</a>

                        </li>
                        {{--<li class="menu-item menu-item-has-children"><a href="#">Shop</a>--}}
                            {{--<ul class="sub-menu">--}}
                                {{--<li class="menu-item"><a href="product-listing.html">Product Listing</a></li>--}}
                                {{--<li class="menu-item"><a href="single-product.html--}}
{{--">Single Product</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        <li class="menu-item "><a href="/contact">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
</body>
