<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Saffron Indian Cuisine</title>

    <link rel="stylesheet"  href="css/kube.css">
    <link rel="stylesheet"  href="css/font-awesome.css">
    <link rel="stylesheet"  href="css/owl.carousel.min.css">
    <link rel="stylesheet"  href="css/style.css">

</head>

<body>
    @include('navbar.navbar')
    @yield('content')
    @include('navbar.footer')

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHg6uXhcYmiLTHRSMhMRL_Yjy23PUd-XE"></script>
    <script src="js/jquery.data-parallax.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/kube.js"></script>
    <script src="js/maps.js"></script>
    <script src="js/index.js"></script>
</body>
