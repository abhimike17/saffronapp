@extends('layouts.app')

@section('content')

					<div class="clear"></div>
					</div>

					<div class="mobile_header ">
						<a href="/" class="saffron_image_logo"></a>
						<a href="javascript:void(0)" class="btn_mobile_menu">
							<span class="saffron_menu_line1"></span>
							<span class="saffron_menu_line2"></span>
							<span class="saffron_menu_line3"></span>
						</a>
					</div>

			<div class="col col-3 saffron_header_right">
				<div class="saffron_inner">
					<a href="#">
						<div class="saffron_shopping_cart">

							<div class="saffron_cart_item_counter">0</div>
						</div>
					</a>
				</div>
			</div>

	<div class="saffron_title_block saffron_corners">
		<div class="saffron_inner_text">
			<h1>Contact Us</h1>
		</div>
	</div>
	<div class="saffron_container saffron_сontact_us">
		<div class="row gutters">
			<div class="col col-8 saffron_content">
				<div class="row saffron_сontact_block">
					<div class="col col-6 saffron_сontact_block_img"><img src="img/img_1.jpg"  alt=""></div>
					<div class="col col-6 saffron_сontact_block_content">
						<h5>Greenville</h5>
						<p>Suite 16, WoodRuff Gallery 1178 WoodRuff RD </p>
						<p>+1 215 456 15 15</p>
						<p class="saffron_сontact_mail">info@saffrongreenville.com</p>
						<h5>WORKING HOURS</h5>

						<p>Monday – Friday from 8:00 am to 11:30 pm</p>
						<p>Weekends from 9:00 am to 11:00 pm</p>
						<a class="saffron_button" href="https://www.google.com/maps/place/Saffron+Indian+Cuisine/@34.8241324,-82.2956559,15z/data=!4m2!3m1!1s0x0:0x92b26e34631601c4?sa=X&ved=2ahUKEwjIuOztrZrdAhUMXKwKHbumB-sQ_BIwCnoECAoQCw">Get Directions<i class="fa fa-angle-right" aria-hidden="true"></i></a>
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection
