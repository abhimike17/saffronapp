@extends('layouts.app')

@section('content')

    <title>Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Appetizers</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>

    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE SAMOSA (TWO PCS.)</span></h5>
                            <div class="saffron_prod_list_price">$3.50</div>
                            <p class="saffron_prod_list_text">
                                Lightly spiced turnovers stuffed with potatoes and green peas</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>MEAT SAMOSA (TWO PCS.)</span></h5>
                            <div class="saffron_prod_list_price">$4.95</div>
                            <p class="saffron_prod_list_text">Lightly spiced turnovers stuffed with minced lamb, herbs and spices</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE PAKORA</span></h5>
                            <div class="saffron_prod_list_price">$3.50</div>
                            <p class="saffron_prod_list_text">Fresh cut vegetables, dipped in lightly chick-pea batter mix and deep fried</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ONION BHAJIA</span></h5>
                            <div class="saffron_prod_list_price">$3.25</div>
                            <p class="saffron_prod_list_text">Crispy onion in lightly seasoned lentil flour batter and deep fried</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHILLY PAKORA</span></h5>
                            <div class="saffron_prod_list_price">$4.95</div>
                            <p class="saffron_prod_list_text">Chilly pepper filled with chef special spices deep frid in chick-pea batter mix</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>TANDOORI ASSORTED APPETIZER
</span></h5>
                            <div class="saffron_prod_list_price">$9.95</div>
                            <p class="saffron_prod_list_text">Combination of tandoori chicken, chicken tikka and seekh kebab</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETARIAN ASSORTED APPETIZER
</span></h5>
                            <div class="saffron_prod_list_price">$6.75</div>
                            <p class="saffron_prod_list_text">Combination of vegetable samosa and vegetable pakora (serves two)</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE KABAB</span></h5>
                            <div class="saffron_prod_list_price">$5.95</div>
                            <p class="saffron_prod_list_text">Assorted ground vegetables & cheese marinated with spiced herbs and deep fried</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>FISH OR CHICKEN PAKORA</span></h5>
                            <div class="saffron_prod_list_price">$5.95</div>
                            <p class="saffron_prod_list_text">Tender strips of fish or chicken with seasoned batter mix and deep fried</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP PAKORA</span></h5>
                            <div class="saffron_prod_list_price">$8.95</div>
                            <p class="saffron_prod_list_text">Succulent deep frid in lighlty spiced chick-pea batter mix</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHANA PAPARI CHAT
</span></h5>
                            <div class="saffron_prod_list_price">$5.95</div>
                            <p class="saffron_prod_list_text">Spiced chick peas, potatoes and flour dumplings delicately tossed in our own sweet and sour tangy sauce</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>BHEL MIX
</span></h5>
                            <div class="saffron_prod_list_price">$3.95</div>
                            <p class="saffron_prod_list_text">Mixture of puffed rice, grain flour sev, crushed poories, tomatoes and onions mixed with sweet and sour chutneys</p>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>


            <div>
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/appetizer1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/appetizer2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/appetizer3.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

@endsection
