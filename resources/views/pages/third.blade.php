@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Tandoori</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>


    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN TIKKA
</span></h5>
                            <div class="saffron_prod_list_price">$13.95</div>
                            <p class="saffron_prod_list_text">
                                Boneless breast of chicken marinated in yogurt, lemon & spices, then cooked in clay oven</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>TANDOORI SHRIMP
</span></h5>
                            <div class="saffron_prod_list_price">$15.95</div>
                            <p class="saffron_prod_list_text">Shrimp marinated in delicately spiced yougurt and baked on skewer in tandoor.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP & KALMI CHICKEN
</span></h5>
                            <div class="saffron_prod_list_price">$15.95</div>
                            <p class="saffron_prod_list_text">Shrimp & Pieces of chicken breast marinated with fresh ground spices, saffron & grilled in tandoor.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>BOTI KEBAB
</span></h5>
                            <div class="saffron_prod_list_price">$15.95</div>
                            <p class="saffron_prod_list_text">Chunks of lean lamb marinated in spices and herbs then baked to perfection</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>TANDOORI CHICKEN

</span></h5>
                            <div class="saffron_prod_list_price">$11.95</div>
                            <p class="saffron_prod_list_text">Tender chicken (with bones) marinated in yougurt & spices, baked on skewers in tandoor</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>TANDOORI SALMON TIKKA
</span></h5>
                            <div class="saffron_prod_list_price">$16.95</div>
                            <p class="saffron_prod_list_text">Thick pieces of fresh salmon filet marinated and baked in tandoor, serverd with green vegetables on sizzler</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>TANDOORI MIXED GRILL
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">An assortment of tandoori specialities like chicken, lamb, shrimp and fish served on sizzler.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SEEKH KEBAB
</span></h5>
                            <div class="saffron_prod_list_price">$15.95</div>
                            <p class="saffron_prod_list_text">Deliciously spieced ground lamb with onions, cilatro, green peppers, marinated with herbs & spices & cooked on skew in tandoor.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>TANDOORI STEAK AND SHRIMP
</span></h5>
                            <div class="saffron_prod_list_price">$16.95</div>
                            <p class="saffron_prod_list_text">Tender juicy steak & shrimp marinated in special recipe & broiled to the peak of flavor & served on sizzler with vegetables.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/tandoori1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/appetizer1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/tandoori2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

@endsection
