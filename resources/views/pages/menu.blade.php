@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

                    <div class="clear"></div>
            </div>
            <div class="mobile_header ">
                <a href="/" class="saffron_image_logo"></a>
                <a href="javascript:void(0)" class="btn_mobile_menu">
                    <span class="saffron_menu_line1"></span>
                    <span class="saffron_menu_line2"></span>
                    <span class="saffron_menu_line3"></span>
                </a>
            </div>
        </div>
        <div class="col col-3 saffron_header_right">
            <div class="saffron_inner">

            </div>
        </div>
    </div>
</header>
<div class="saffron_title_block saffron_corners">
    <div class="saffron_inner_text">
        <h1>CHOOSE OUR AWESOME LUNCH MENU</h1>
    </div>
</div>
<div class="row">
    <div class="col col-12 saffron_content">
        <div class="saffron_menu2_block">
            <div class="saffron_container">
                <div class="row gutters saffron_menu2_1_block">
                    <div class="col col-6">
                        <div class="saffron_menu2_block_item">
                            <div class="saffron_prod_list_image_cont">
                                <div class="saffron_prod_list_image_wrapper">
                                    <div class="saffron_team_overlay"></div>
                                    <img src="img/11-600x600.png" alt="">
                                    <a class="saffron_add_to_cart_button" href="#"></a>
                                </div>
                            </div>
                            <div class="saffron_prod_list_cont">
                                <h5><a href="#">Weekdays</a></h5>
                                <div class="saffron_best_offer_field">Monday -to- Friday,   11:30 AM EST - 2:30 PM EST</div>
                                <p class="saffron_prod_list_text">Variety of dishes, includes curries, rice entrees, bread and various dishes from our signature menu</p>
                                <div class="saffron_prod_list_price">$9.95</div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-6">
                        <div class="saffron_menu2_block_item">
                            <div class="saffron_prod_list_image_cont">
                                <div class="saffron_prod_list_image_wrapper">
                                    <div class="saffron_team_overlay"></div>
                                    <img src="img/10-600x600.png" alt="">
                                    <a class="saffron_add_to_cart_button" href="#"></a>
                                </div>
                            </div>
                            <div class="saffron_prod_list_cont">
                                <h5><a href="#">Weekends</a></h5>
                                <div class="saffron_best_offer_field">Saturday -&- Sunday,   12:00 PM EST - 3:00 PM EST</div>
                                <p class="saffron_prod_list_text">Try our special unique weekend buffet.
We add a new dish every weekend with selections from various ethnic Indian cuisines</p>
                                <div class="saffron_prod_list_price">$10.95</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="saffron_title_block saffron_corners">
                    <div class="saffron_inner_text">
                        <h1>Special Luncheon Menu</h1>
                    </div>
                </div>
                <h6>(Available Weekdays Only)</h6>
                <h7>Mon-Fri, 11:30 AM - 2:30 PM</h7>

                <div class="saffron_menu2_2_block">
                    <div class="row gutters">
                        <div class="col col-6">
                            <ul class="saffron_menu2_2_block_item">
                                <li class="saffron_prod_list_cont">
                                    <a href="#">
                                        <h5 class="saffron_prod_list_title"><span>Curry Lunch</span></h5>
                                        <div class="saffron_prod_list_price">Chicken: $8.95 Lamb:$9.95</div>
                                        <p class="saffron_prod_list_text">
Choice of chicken, lamb or beef curry, served with basmati rice and naan bread</p>
                                    </a>
                                </li>
                                <li class="saffron_prod_list_cont">
                                    <a href="#">
                                        <h5 class="saffron_prod_list_title"><span>MASALA SPECIAL</span></h5>
                                        <div class="saffron_prod_list_price">Chicken: $8.95 Lamb:$9.95</div>
                                        <p class="saffron_prod_list_text">Choice of chicken or lamb masala, served with basmati rice and naan bread.</p>
                                    </a>
                                </li>
                                <li class="saffron_prod_list_cont">
                                    <a href="#">
                                        <h5 class="saffron_prod_list_title"><span>VEGETARIAN LUNCH</span></h5>
                                        <div class="saffron_prod_list_price">$8.95</div>
                                        <p class="saffron_prod_list_text">Choice of Palak Panir OR Veg Korma OR chana masala served with rice and naan bread.</p>
                                    </a>
                                </li>
                                <li class="saffron_prod_list_cont">
                                    <a href="#">
                                        <h5 class="saffron_prod_list_title"><span>TANDOORI LUNCH (Low Calorie)</span></h5>
                                        <div class="saffron_prod_list_price">$10.95</div>
                                        <p class="saffron_prod_list_text">Tandoori chicken, fish tikka served with a tossed salad, basmati rice and roti bread.</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col col-6">
                            <ul class="saffron_menu2_2_block_item">
                                <li class="saffron_prod_list_cont">
                                    <a href="#">
                                        <h5 class="saffron_prod_list_title"><span>SEAFOOD SPECIAL LUNCH</span></h5>
                                        <div class="saffron_prod_list_price">$10.95</div>
                                        <p class="saffron_prod_list_text">Shrimp OR scallop masala OR korma sauce with basmati rice and naan bread.</p>
                                    </a>
                                </li>
                                <li class="saffron_prod_list_cont">
                                    <a href="#">
                                        <h5 class="saffron_prod_list_title"><span>SAFFRON SPECIAL LUNCH</span></h5>
                                        <div class="saffron_prod_list_price">$10.95</div>
                                        <p class="saffron_prod_list_text">Tandoori chicken or lamb or beef curry served with basmati rice and naan bread.</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div >
                            <div class="row">
                                <div class="col col-12 saffron_content">
                                    <div class="saffron_container saffron_team">
                                        <div class="row gutters">
                                            <div class="col col-4">
                                                <div class="saffron_team_item">
                                                    <div class="saffron_team_image">
                                                        <img src="img/team_1-1600x1600.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-4">
                                                <div class="saffron_team_item">
                                                    <div class="saffron_team_image">
                                                        <img src="img/team_2-1600x1600.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-4">
                                                <div class="saffron_team_item">
                                                    <div class="saffron_team_image">
                                                        <img src="img/team_3-1600x1600.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>

            </div>
        </div>

    </div>
</div>

@endsection
