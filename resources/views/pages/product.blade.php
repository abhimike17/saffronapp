@extends('layouts.app')

@section('content')

                <div id="tab1">
                    <p>Classic marinara sauce, authentic old-world pepperoni, all-natural Italian sausage, slow-roasted ham, hardwood smoked bacon, seasoned pork and beef. Best an our Hand Tossed crust. With more than 50 years of experience under our belts, we understand how to best serve our customers through tried and true service principles. Instead of following trends, we set them. We create food we’re proud to serve and deliver it fast, with a smile.</p>
                </div>
                <div id="tab2">
                    <ul class="saffron_tab2">
                        <li><span>Weight</span>2 kg</li>
                        <li><span>Dimensions</span>26 x 26 x 5.5 cm</li>
                    </ul>
                </div>
                <div id="tab3">
                    <div class="saffron_tab3">
                        <p>There are no reviews yet.</p>
                        <h5 class="saffron_form_title">BE THE FIRST TO REVIEW “saffron”</h5>

                        <div class="saffron_form_block ">
                            <p class="saffron_comment_notes">Your email address will not be published. Required fields are marked *</p>
                            <ul class="saffron_stars">
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                            </ul>
                            <form  class="saffron_form">
                                <textarea class="form_comment" name="message" ></textarea>
                                <input type="text" class="form_user-name " name="name" >
                                <input type="text" class="form_email" name="email" >
                                <input class="" type="submit" value="Submit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="saffron_product_listing_content">
                <h2>Related products</h2>
                <div class="saffron_product_listing_item">
                    <div class="saffron_product_wrapper">
                        <a href="product.html">
                            <img src="img/11-300x300.png" alt="">
                            <h5 class="saffron_prod_list_title">Margherita</h5>
                            <p class="saffron_prod_list_text">Classic marinara sauce, authentic old-world pepperoni, all-natural Ita</p>
                            <div class="saffron_prod_list_price"><span>$</span>2.60</div>
                        </a>
                        <a class="saffron_button" href="product.html">Add to cart<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>

                <div class="saffron_product_listing_item">
                    <div class="saffron_product_wrapper">
                        <a href="product.html">
                            <img src="img/5-300x300.png" alt="">
                            <h5 class="saffron_prod_list_title">Fortezza</h5>
                            <p class="saffron_prod_list_text">Classic marinara sauce, authentic old-world pepperoni, all-natural Ita</p>
                            <div class="saffron_prod_list_price"><span>$</span>3.50</div>
                        </a>
                        <a class="saffron_button" href="product.html">Add to cart<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>

                <div class="saffron_product_listing_item">
                    <div class="saffron_product_wrapper">
                        <a href="product.html">
                            <img src="img/2-300x300.png" alt="">
                            <h5 class="saffron_prod_list_title">Pepperoni</h5>
                            <p class="saffron_prod_list_text">Classic marinara sauce, authentic old-world pepperoni, all-natural Ita</p>
                            <div class="saffron_prod_list_price"><span>$</span>2.90</div>
                        </a>
                        <a class="saffron_button" href="product.html">Add to cart<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-4 saffron_sidebar">
            <div class="saffron_sidebar_block saffron_sidebar_categories">
                <h5>Menu Categories</h5>
                <ul>
                    <li><a href="product-listing.html">Burgers</a></li>
                    <li><a href="product-listing.html">Desserts</a></li>
                    <li><a href="product-listing.html">Drinks</a></li>
                    <li><a href="product-listing.html">Pasta</a></li>
                    <li><a href="product-listing.html">Pizzas</a></li>
                    <li><a href="product-listing.html">Salads</a></li>
                </ul>
            </div>

            <div class="saffron_sidebar_block saffron_sidebar_tags">
                <h5>Tags</h5>
                <ul>
                    <li><a href="product-listing.html">Burger</a></li>
                    <li><a href="product-listing.html">Dessert</a></li>
                    <li><a href="product-listing.html">Drink</a></li>
                    <li><a href="product-listing.html">Pasta</a></li>
                    <li><a href="product-listing.html">Pizza</a></li>
                    <li><a href="product-listing.html">Salad</a></li>
                </ul>
            </div>
            <div class="saffron_sidebar_block saffron_featured_posts">
                <h5>FEATURED MENU ITEMS</h5>
                <div class="saffron_featured_item">
                    <a  href="standard-post.html">
                        <img src="img/11-180x180.png" alt="">
                        <span>Margherita</span>
                    </a>
                    <div class="saffron_featured_item_price">$2.60</div>
                </div>
                <div class="saffron_featured_item">
                    <a href="standard-post.html">
                        <img src="img/10-180x180.png" alt="">
                        <span>saffron</span>
                    </a>
                    <div class="saffron_featured_item_price">$2.00 – $12.00</div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="saffron_back_to_top"></div>
@endsection
