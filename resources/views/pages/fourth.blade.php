@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Sea Food</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>

    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>FISH OR SHRIMP OR SCALLOP MASALA
</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">
                                Fresh salmon or shrimp or scallops in tandoori style, cooked in rich creamy tomato sauce garnished with fresh cilantro.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP OR SCALLOP MANGO
</span></h5>
                            <div class="saffron_prod_list_price">Chicken: $14.95 Lamb:$9.95</div>
                            <p class="saffron_prod_list_text">Shrimp or scallops simmered with sweet & sour mango sauce, with scallions and spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETARIAN LUNCH</span></h5>
                            <div class="saffron_prod_list_price">$8.95</div>
                            <p class="saffron_prod_list_text">Choice of Palak Panir OR Veg Korma OR chana masala served with rice and naan bread.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP OR SCALLOP JALFRAZE
</span></h5>
                            <div class="saffron_prod_list_price">$15.95</div>
                            <p class="saffron_prod_list_text">Shrimps or scallops sauteed with asparagus, scallion, green peppers, onion, tomatoes, herbs and spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP SAAG

</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">Shrimps cooked with genereous portion of spinach with spices & herbs.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SEAFOOD SPECIAL LUNCH</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Shrimp OR scallop masala OR korma sauce with basmati rice and naan bread.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP KORMA
</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">Scallop or Shrimp cooked with mild spices, herbs, nuts & raisins in a creamy sauce.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SEAFOOD OR SHRIMP VINDALLO

</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">Shrimps, scallops and fish or only shrimp cooked in a spicy curry sauce with potatoes, tomatoes, cilantro and splash of lemon & vinegar.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP COCONUT CURRY

</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">Jumbo shrimps simmered in coconut curry sauce.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CRAB MASALA

</span></h5>
                            <div class="saffron_prod_list_price">$15.95</div>
                            <p class="saffron_prod_list_text">Crab cooked in tomato creamy sauce with herbs.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_1-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_2-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_3-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>


@endsection
