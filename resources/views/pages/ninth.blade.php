@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Deserts & Side Orders</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>


    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <h2>Desserts</h2>
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>KULFI (PISTACHIOS OR MANGO)
</span></h5>
                            <div class="saffron_prod_list_price">$3.75</div>
                            <p class="saffron_prod_list_text">
                                Homemade Indian ice cream made with milk and nuts or milk and mangoes.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>GULAB JAMUN (TWO PCS)
</span></h5>
                            <div class="saffron_prod_list_price">$3.50</div>
                            <p class="saffron_prod_list_text">A north Indian sweet made from essence milk & homemade cheese balls deep fried in butter and soaked in honey syrup.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>KHEER</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">Cooling rice pudding flavored with cardamom and garnished with Pistachios.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>FRUIT FIRNI
</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">Traditional Indian favorite custard, made with chef's special recipe topped with fruits.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>GAJAR HALWA with ICE CREAM

</span></h5>
                            <div class="saffron_prod_list_price">$3.75</div>
                            <p class="saffron_prod_list_text">Special homemade glazed carrot pudding (warm) topped with vanilla ice cream..</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ICE CREAM

</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">Mango, Vaniall or Chocolate.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHEESE CAKE

</span></h5>
                            <div class="saffron_prod_list_price">$3.75</div>
                            <p class="saffron_prod_list_text">New York style cheesecake.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <h2>Side Orders</h2>
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ACHAR</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>
                            <p class="saffron_prod_list_text">Hot & Spicy mixed Indian pickles.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>RAITA
</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>
                            <p class="saffron_prod_list_text">Refreshing yougurt with shredded cucumbers, roasted cumin & herbs.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>RAITA
</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>
                            <p class="saffron_prod_list_text">Refreshing yougurt with shredded cucumbers, roasted cumin & herbs.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>PLAIN YOUGURT

</span></h5>
                            <div class="saffron_prod_list_price">$1.75</div>
                            <p class="saffron_prod_list_text">Homemade plain yougurt.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>MANGO CHUTNEY

</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>
                            <p class="saffron_prod_list_text">Sweet & mildly spiced mangoes and herbs.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ROASTED PAPPAD

</span></h5>
                            <div class="saffron_prod_list_price">$1.75</div>
                            <p class="saffron_prod_list_text">Thin & crispy lightly spiced lentil wafer with black pepper.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ONION CHUTNEY

</span></h5>
                            <div class="saffron_prod_list_price">$1.75</div>
                            <p class="saffron_prod_list_text">Spicy onion and tomato relish.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_1-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_2-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_3-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

@endsection
