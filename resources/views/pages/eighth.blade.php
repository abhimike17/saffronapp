@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Breads</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>

    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>NAAN</span></h5>
                            <div class="saffron_prod_list_price">$2.50</div>
                            <p class="saffron_prod_list_text">
                                Traditional punjabi unleavened white flour bread baked in our tandoor.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>GARLIC NAAN</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">Unleavened white flour bread baked with fresh garlic and cilantro.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>PARATHA</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">Multilayered whole wheat bread topped with butter.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ROTI</span></h5>
                            <div class="saffron_prod_list_price">$2.50</div>
                            <p class="saffron_prod_list_text">Whole wheat unleavened bread baked in our clay oven.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHILLY NAAN
</span></h5>
                            <div class="saffron_prod_list_price">$2.50</div>
                            <p class="saffron_prod_list_text">Whole wheat unleavened bread baked in our clay oven.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ROTI</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">Unleavened white flour bread topped with spicy chilies.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ONION PANEER / ONION / ALOO KULCHA
</span></h5>
                            <div class="saffron_prod_list_price">$3.50</div>
                            <p class="saffron_prod_list_text">Naan bread filled with spiced onions & cheese or spiced onions or mildly spiced potatoes.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>PESHWARI NAAN
</span></h5>
                            <div class="saffron_prod_list_price">$4.50</div>
                            <p class="saffron_prod_list_text">Unleavened white flour bread stuffed with cherries and slightly sweet coconut, baked in our clay oven.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>KEEMA NAAN
</span></h5>
                            <div class="saffron_prod_list_price">$4.50</div>
                            <p class="saffron_prod_list_text">White flour bread stuffed with minced chicken mildy spiced.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ASSORTED BASKET OF BREAD

</span></h5>
                            <div class="saffron_prod_list_price">$7.50</div>
                            <p class="saffron_prod_list_text">Perfect combination of breads from our clay oven like garlic naan, roti and plain naan.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_1-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_2-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_3-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

@endsection
