@extends('layouts.app')

@section('content')
<body class="saffron_about1">
<header class="saffron_header">
                <div class="clear"></div>
            {{--</div>--}}
            <div class="mobile_header ">
                <a href="index.html" class="saffron_image_logo"></a>
                <a href="javascript:void(0)" class="btn_mobile_menu">
                    <span class="saffron_menu_line1"></span>
                    <span class="saffron_menu_line2"></span>
                    <span class="saffron_menu_line3"></span>
                </a>
            </div>
        {{--</div>--}}
        {{--<div class="col col-3 saffron_header_right">--}}
            {{--<div class="saffron_inner">--}}
                {{--<a href="#">--}}
                    {{--<div class="saffron_shopping_cart">--}}
                        {{--<div class="saffron_total_price">$0.00</div>--}}
                        {{--<div class="saffron_total_items">0 items - View Cart</div>--}}
                        {{--<div class="saffron_cart_item_counter">0</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</header>
<div class="saffron_title_block saffron_corners">
    <div class="saffron_inner_text">
        <h1>Saffron Indian Cuisine</h1>
    </div>
</div>
<div >
    <div class="row">
        <div class="col col-12 saffron_content">
            <div class="owl-carousel saffron_slider1i">
                <div class="owl-item">
                    <img src="img/about_1-1.jpg" alt="">
                </div>
                <div class="owl-item">
                    <img src="img/about_2-1.jpg" alt="">
                </div>
                <div class="owl-item">
                    <img src="img/about_3-1.jpg" alt="">
                </div>
            </div>
            <div class="saffron_container saffron_content_title_block">
                <h2>We Are saffron Indian Cuisine</h2>
                <p>From traditional curries that are the embodiment of Indian food, to more exotic dishes for those who appreciate innovation and crave a truly international flavor, we prepare everything to rave reviews. In addition, even beyond an unforgettable meal from our signature menu, Safron offers guests something more: a unique and upscale dining experience characterized by warmth and beauty </p>
                <img src="img/separator_light.png" alt="">
                <p>Our vision is to simplify Indian cooking and bring you back to the basics with nutrition, quality, flavor, aroma and taste.  our spice blends fresh ingredients and your imagination will transform your craving into simple and satisfying cuisine.</p>
            </div>
            <div class="saffron_container saffron_branches">
                <div class="row gutters">
                    <div class="col col-6 saffron_branch">
                        <img src="img/img_6-1024x801.jpg" alt="">
                        {{--<h4>Gree</h4>--}}
                        {{--<p>St Johns Pl/Nostrand Av, Brooklyn, NY 11216, USA</p>--}}
                        {{--<p>+1 215 456 15 15 brooklyn@saffron.com</p>--}}
                        {{--<a class="saffron_button" href="https://www.google.com/maps/place/1178+Woodruff+Rd+%2316,+Greenville,+SC+29607/@34.8242028,-82.296994,17z/data=!3m1!4b1!4m5!3m4!1s0x885828ce935aa3e5:0x1b23d9f1427abad6!8m2!3d34.8242028!4d-82.2948053">Get Directions<i class="fa fa-angle-right" aria-hidden="true"></i></a>--}}
                    </div>
                    <div class="col col-6 saffron_branch">
                        <img src="img/img_5-1024x801.jpg" alt="">
                        {{--<h4>Queens</h4>--}}
                        {{--<p>Hillside Av/162 St, Queens, NY, Queens, New York 11432, USA </p>--}}
                        {{--<p>+1 079 385 4690 queens@saffron.com</p>--}}
                        {{--<a class="saffron_button" href="#">Get Directions<i class="fa fa-angle-right" aria-hidden="true"></i></a>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="saffron_testimonials">
                <div class="saffron_container">
                    <h6>Exquisite Dining, Tantalizing Flavors</br>A Step Above The Rest</h6>
                    <img class="saffron_single_img" src="img/separator_dark.png" alt="">
                    <div class="owl-carousel saffron_slider1i_auto_height">
                        <div class="saffron_testimonials_item">
                            <h1>What is Saffron?</h1>

                            <p>The Sanskrit name for saffron points to the ancient Indian area of saffron production: Kashmirajanman or "product of Kashmir".  Saffron in some modern Indian languages is called kesar (Hindi) and Kisar (Urdu).  the Saffron filaments or threads are actually the dried stigmas of the saffron flower,  "Crocus Sativus Linneaus".  Each flower contains only three stigmas.  These threads must be picked from each flower by hand and more than 75,000 of these flowers are needed to produce just one pound of Saffron filaments making it the world's most precious spice.  But, because of saffron's strong coloring power and intense flavor it can be used sparingly.  Saffron is used both for its bright orange-yellow color and for its strong, intense flavor and aroma.</p>
                            {{--<div class="saffron_testimonials_author_cont">--}}
                                {{--<img   src="img/testimonial_1-200x200.jpg" alt="">--}}

                            {{--</div>--}}
                        </div>
                        <div class="saffron_testimonials_item">
                            <h1>Saffron in news</h1>

                            <p>Dining Diva Reed Messer's blog in Greenville News.</p>
                            <p>"Open for only a few months now, Saffron is an upscale restaurant that focuses mostly on North Indian cuisine.  The place is decorated to the hilt"  >>>more>>> </p>
                            <p>Saffron has been rated as the best Indian cuisine in
                                Greenville upstate</p>
                            <p>User have rated five stars for our restaurant on "10best.com". Editors of 10Best.com, recommends restaurants, hotels, nightlife and attractions across all cities in US.</p>
                            <p>Saffroon is proud to beChamber of Commerce,Greenville, SC member. We are proud to be assosciated with Chamber of commerce, Greenville SC. in serving the upstate community. </p>
                            {{--<div class="saffron_testimonials_author_cont">--}}
                                {{--<img src="img/testimonial_2-200x200.jpg" alt="">--}}
                        {{--</div>--}}
                        <div class="saffron_testimonials_item">

                            <div class="saffron_testimonials_author_cont">
                                <img src="img/testimonial_3-200x200.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            {{--<div class="saffron_advantages">--}}
                {{--<div data-parallax='{"y":"45vh","scale":1,"rotate":0,"opacity":1}' class="saffron_parallax_background"></div>--}}
                {{--<div class="saffron_container">--}}
                    {{--<div class="row gutters">--}}
                        {{--<div class="col col-4">--}}
                            {{--<div class="saffron_advantages_item saffron_icon_box">--}}
                                {{--<img src="img/icon_1.png" alt="">--}}
                                {{--<h4>Quality Foods</h4>--}}
                                {{--<p>Sit amet, consectetur adipiscing elit quisque eget maximus velit, non eleifend libero curabitur dapibus mauris sed leo cursus aliquetcras suscipit.</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col col-4">--}}
                            {{--<div class="saffron_advantages_item saffron_icon_box">--}}
                                {{--<img src="img/icon_3.png" alt="">--}}
                                {{--<h4>Fastest Delivery</h4>--}}
                                {{--<p>Sit amet, consectetur adipiscing elit quisque eget maximus velit, non eleifend libero curabitur dapibus mauris sed leo cursus aliquetcras suscipit.</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col col-4">--}}
                            {{--<div class="saffron_advantages_item saffron_icon_box">--}}
                                {{--<img src="img/icon_2.png" alt="">--}}
                                {{--<h4>Original Recipes</h4>--}}
                                {{--<p>Sit amet, consectetur adipiscing elit quisque eget maximus velit, non eleifend libero curabitur dapibus mauris sed leo cursus aliquetcras suscipit.</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
<div class="saffron_back_to_top"></div>
</div>
</body>
@endsection
