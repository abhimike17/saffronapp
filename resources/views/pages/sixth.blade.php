@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Vegan & Vegetarian</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>

    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE KORMA
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">
                                Variety of fresh vegetables cooked in light cream sauce & special blend of spices, garnished with nuts.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE MASALA
VEGETABLE MASALA
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Assorted of fresh vegetables cooked in mildly spiced tomato & cream sauce with herbs, spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>PANIR MAKHNI
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">CHomemade cheese cooked in appetizing tomato and butter sauce with fresh herbs.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>MALAI KOFTA
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Vegetable & Home made cheese balls, cooked in onion and creamy sauce with almonds, cashews and raisins.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>EGGPLANT BHARTHA (VEGAN)
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Eggplant baked in clay oven then mashed & sauteed with onion, tomatoes, ginger and scallions.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ALLO GOBHI (VEGAN)
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Cauliflower and potatoes cooked with onion, green peppers, tomato hebs & spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHANA MASALA (VEGAN)

</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Garbanzo beans cooked in north Indian style sauce.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE MANGO(VEGAN)
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Fresh garden vegetables cooked with sweet & sour mangoes, spices and herbs with touch of ginger & scallions.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>PANIR KRAHI

</span></h5>
                            <div class="saffron_prod_list_price">$11.95</div>
                            <p class="saffron_prod_list_text">Homemade cheese cubes sauteed in a wok with onions, peppers, tomato, ginger, garlic & spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>BHINDI MASALA (VEGAN)
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Fresh baby okra sauteed with onion, tomato, green pepper, cilantro, fresh herbs & spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>MUSHROOM BROCCOLI KRAHI (VEGAN)
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Fresh mushroom & broccoli sauteed in a wok with onions, tomatoes and green pepper with fresh herbs and spices.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SAAG PANIR or ALLO SAAG
</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Spinach and homemade cheese cubes or potatoes cooked in mild spices & herbs.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE JALFRAZIE
</span></h5>
                            <div class="saffron_prod_list_price">$11.95</div>
                            <p class="saffron_prod_list_text">Assorted vegetables sauteed with asparagus, scallion, green pepper, homemade cheese, onion, tomatoes, herbs & spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>MATAR PANIR or ALLO MATAR

</span></h5>
                            <div class="saffron_prod_list_price">$10.95</div>
                            <p class="saffron_prod_list_text">Homemade cheese and peas or poratoes cooked in a mildly spiced tomato and cream sauce.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>DAL MAKHNI or DAL TARKA
</span></h5>
                            <div class="saffron_prod_list_price">$9.95</div>
                            <p class="saffron_prod_list_text">Variety of lentils, slow simmered and sauteed with ginger, tomato, fresh herbs & spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>JEERA ALLO OR ALLO METHI (VEGAN)
</span></h5>
                            <div class="saffron_prod_list_price">$9.95</div>
                            <p class="saffron_prod_list_text">Potatoes sauteed in a wok with onions, tomatoes & ginger, cumin or fenugreek, herbs & spcies.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>PANIR BHURJI

</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Homemade shredded cheese cooked with tomatoes, onions, peppers and spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHAHI PANIR

</span></h5>
                            <div class="saffron_prod_list_price">$11.95</div>
                            <p class="saffron_prod_list_text">Homemade cheese cooked in a mildy creamy sauce with green peppers, onions, tomatoes with herbs and spices.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_1-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_2-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_3-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>



@endsection
