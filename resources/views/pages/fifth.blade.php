@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Chicken Entrees</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>

    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN TIKKA MASALA
</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">
                                Tender pieces of chicken marinated & roasted in tandoor, then simmered in tomato & cramy sauce.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN JALFRAZE
</span></h5>
                            <div class="saffron_prod_list_price">$13.95</div>
                            <p class="saffron_prod_list_text">Tender pieces of chicken breast sauteed with asparagus, scallion, green pepper, onion, tomatoes, herbs & spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN SAAG
</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Boneless chicken cooked with spinach and flavored with freshly ground ginger, garlic and spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN KORMA
</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Boneless chicken cooked in creamy sauce with mild spices, herbs, nuts & raisins.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN MANGO

</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Boneless chicken cooked with sweet & sour mango sauce and spices topped with scallions.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN CURRY

</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Classic Northern Indian style chicken dish cooked in thick curry sauce.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN VINDALLO
</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Classic southern Indian style Chicken cooked in tangy & highly spiced sauce with potatoes.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN MAKHNI
</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Boneless chicken marinated and roasted in tandoor and cooked in rich tomato/ butter sauce with exotic Indian spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>KASHMIRI MURG

</span></h5>
                            <div class="saffron_prod_list_price">$13.95</div>
                            <p class="saffron_prod_list_text">Kashmiri speciality chicken roasted in tandoor and simmered in mild creamy sauce with fruits.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN KADAI

</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Chicken sauteed in wok with onions, peppers, gigner, garlic and herbs.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN COCONUT CURRY

</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Boneless chicken cooked in coconut curry sauce.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/chicken1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/appetizer1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/chicken2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>



@endsection
