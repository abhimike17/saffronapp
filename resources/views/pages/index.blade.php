@extends('layouts.app')

@section('content')
    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="index.html" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="row">
        <div class="col col-12 saffron_content saffron_corners">
            <div class="owl-carousel saffron_slider1i saffron_slider_main ">

                <div class="owl-item saffron_slider_main_item saffron_slider_main_item1">
                    <div class="saffron_slider_main_item_img">
                        <div class=""><img src="img/food_2.png" alt=""></div>
                    </div>
                    <div class="saffron_slider_main_item_cont">
                        <div class="saffron_item_cont">
                            <h1>Saffron Indian Cuisine.</h1>
                            <h2>Exquisite Dining, Tantalizing Flavors </h2>
                            <div class="saffron_slider_main_item_text"></div>
                            <a class="saffron_button" href="/contact">Contact Us<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            <a class="saffron_button saffron_button_background" href="/menu">View Menu<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <div class="owl-item saffron_slider_main_item saffron_slider_main_item1 saffron_slider_main_item2">
                    <div class="saffron_slider_main_item_img">
                        <div class=""><img src="img/food_3.png" alt=""></div>
                    </div>
                    <div class="saffron_slider_main_item_cont">
                        <div class="saffron_item_cont">
                            <h1>Saffron Indian Cuisine.</h1>
                            <h2>Exquisite Dining, Tantalizing Flavors </h2>
                            <div class="saffron_slider_main_item_text"></div>
                            <a class="saffron_button" href="/contact">Contact Us<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            <a class="saffron_button saffron_button_background" href="/menu">View Menu<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>
                <div class="saffron_slider_main_item saffron_slider_main_item1 saffron_slider_main_item2 saffron_slider_main_item3">
                    <img src="img/slide_2.jpg" alt="">
                    <div class="saffron_slider_main_item_cont">
                        <div class="saffron_item_cont">
                            <h2>Exquisite Dining, Tantalizing Flavors </h2>
                            <h1>Saffron Indian Cuisine.</h1>
                            <div class="saffron_slider_main_item_text"></div>
                            <a class="saffron_button saffron_button_background" href="/menu">View Menu<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        <div class="row saffron_block_2col">
            <div class="col col-6">
                <div class="saffron_content_title_block">
                    <h2>We Are saffron</h2>
                    <h6>Our vision is to simplify Indian cooking and bring you back to the basics with nutrition, quality, flavor, aroma and taste.  our spice blends fresh ingredients and your imagination will transform your craving into simple and satisfying cuisine.  </h6>
                    <img src="img/separator_light.png" alt="">
                    <h2></h2>
                    <img src="img/separator_light.png" alt="">
                    <p></p>
                    <h3></h3>
                </br>
                    <p class="saffron_prod_list_text"></p>

                    <a class="saffron_button" href="/about">More About Us<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    <a class="saffron_button saffron_button_background" href="/menu">View Menu<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col col-6">
                <div class="saffron_block_2col_img saffron_block_2col_img3"></div>
            </div>
        </div>
        <div class="saffron_menu2_block saffron_menu2_block_home">

            <div class="saffron_container">
                <h2>Timings</h2>
                <div class="row gutters saffron_menu2_1_block">
                    <div class="col col-6">
                        <div class="saffron_menu2_block_item">
                            <div class="saffron_prod_list_image_cont">
                                <div class="saffron_prod_list_image_wrapper">
                                    <div class="saffron_team_overlay"></div>
                                    <img src="img/11-600x600.png" alt="">
                                    <a class="saffron_add_to_cart_button" href="#"></a>
                                </div>
                            </div>
                            <div class="saffron_prod_list_cont">
                                <h5><a href="/menu">Lunch & Buffet</a></h5>
                                <div class="saffron_best_offer_field">Monday - Friday 11.30 AM - 2.30 PM</div>
                                <div class="saffron_best_offer_field">Saturday & Sunday 12.00 PM - 3.00 PM </div>
                                                             </div>
                        </div>
                    </div>
                    <div class="col col-6">
                        <div class="saffron_menu2_block_item">
                            <div class="saffron_prod_list_image_cont">
                                <div class="saffron_prod_list_image_wrapper">
                                    <div class="saffron_team_overlay"></div>
                                    <img src="img/10-600x600.png" alt="">
                                    <a class="saffron_add_to_cart_button" href="#"></a>
                                </div>
                            </div>
                            <div class="saffron_prod_list_cont">
                                <h5><a href="#">Dinner</a></h5>
                                <div class="saffron_best_offer_field">Sunday - Thursday 5.00 PM - 10.00 PM </div>
                                <div class="saffron_best_offer_field">Friday & Saturday 5.00 PM - 10.30 PM  </div>


                            </div>
                        </div>

                    </div>
                </div>


                <a class="saffron_button saffron_button_background" href="/menu">View Menu<i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="saffron_countdown_section">
            <div class="row saffron_block_2col">
                <div class="col col-6">
                    <div class="saffron_block_2col_img saffron_block_2col_img4"></div>
                </div>
                <div class="col col-6">
                    <div class="saffron_countdown_block">
                        <div class="saffron_content_title_block">
                            <h6>More Info</h6>
                            <h2>Reservations</h2>
                            <img src="img/separator_light.png" alt="">
                            <p>Whether it's big or small group, we can reserve tables for you, so that you do not have to wait.
                                Call us @ 864-288-7400, for reserving your table </p>
                            <h3>NOTE</h3>
                            <p class="saffron_prod_list_text">Expect wait time of 10 to 15 minutes, to be seated. No orders for starters(appetizers) would be taken, 30 minutes before dinner closing hours</p>

                                                    </div>
                    </div>
                </div>

            </div>
        </div>

        {{--<div id="map-canvas" class="canvas_map_container canvas_map_container_home"></div>--}}
    </div>
</div>
<div class="saffron_back_to_top"></div>

@endsection
