@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Soups & Salads</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>

                    <div class="saffron_menu2_2_block">
                        <div class="row gutters">
                            <div class="col col-6">
                                <h2>Soups</h2>
                                <ul class="saffron_menu2_2_block_item">
                                    <li class="saffron_prod_list_cont">
                                        <a href="#">
                                            <h5 class="saffron_prod_list_title"><span>MULLIGATWANY SOUP
</span></h5>
                                            <div class="saffron_prod_list_price">$2.95</div>
                                            <p class="saffron_prod_list_text">
                                                Traditional Indian soup made from lentils and vegetables (pureed).</p>
                                        </a>
                                    </li>
                                    <li class="saffron_prod_list_cont">
                                        <a href="#">
                                            <h5 class="saffron_prod_list_title"><span>CHICKEN SOUP
</span></h5>
                                            <div class="saffron_prod_list_price">$2.95</div>
                                            <p class="saffron_prod_list_text">Delicious chicken soup, with herbs and spices.</p>
                                        </a>
                                    </li>
                                    <li class="saffron_prod_list_cont">
                                        <a href="#">
                                            <h5 class="saffron_prod_list_title"><span>TOMATO SOUP
</span></h5>
                                            <div class="saffron_prod_list_price">$3.50</div>
                                            <p class="saffron_prod_list_text">Tomato soup in Indian style with touch of black pepper and herbs</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col col-6">
                                <h2>Salads</h2>
                                <ul class="saffron_menu2_2_block_item">
                                    <li class="saffron_prod_list_cont">
                                        <a href="#">
                                            <h5 class="saffron_prod_list_title"><span>GARDEN SALAD
</span></h5>
                                            <div class="saffron_prod_list_price">$3.95</div>
                                            <p class="saffron_prod_list_text">Fresh garden vegetables served with your choice of dressing</p>
                                        </a>
                                    </li>
                                    <li class="saffron_prod_list_cont">
                                        <a href="#">
                                            <h5 class="saffron_prod_list_title"><span>TANDOORI TIKKA SALAD
</span></h5>
                                            <div class="saffron_prod_list_price">$5.95</div>
                                            <p class="saffron_prod_list_text">Roasted chicken served over a garden salad mixed with ranch dressing</p>
                                        </a>
                                    </li>
                                    <li class="saffron_prod_list_cont">
                                        <a href="#">
                                            <h5 class="saffron_prod_list_title"><span>KACHUMBER SALAD

</span></h5>
                                            <div class="saffron_prod_list_price">$3.95</div>
                                            <p class="saffron_prod_list_text">Made of diced cucumbers, tomatoes, onions and cilantro with a touch of tangy sauce.</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                            <div >
                                <div class="row">
                                    <div class="col col-12 saffron_content">
                                        <div class="saffron_container saffron_team">
                                            <div class="row gutters">
                                                <div class="col col-4">
                                                    <div class="saffron_team_item">
                                                        <div class="saffron_team_image">
                                                            <img src="img/team_1-1600x1600.jpg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col col-4">
                                                    <div class="saffron_team_item">
                                                        <div class="saffron_team_image">
                                                            <img src="img/team_2-1600x1600.jpg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col col-4">
                                                    <div class="saffron_team_item">
                                                        <div class="saffron_team_image">
                                                            <img src="img/team_3-1600x1600.jpg" alt="">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
@endsection
