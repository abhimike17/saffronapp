
@extends('layouts.app')

@section('content')

    <title>Libations & Bar</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Libations & Bar</h1>
        </div>
    </div>

    </br>


    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <h2>White Wines</h2>
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Madonna Kabinett Riesling, Genmany
</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Black Opal Chardonnay, Australia
</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Giesen Sauvigon Blanc, New Zealand</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Ruffino Lumina Pinot Grigio, Italy
</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SULA Chenin Blanc, India

</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Cartidge & Browne Chardonnay, CA, USA

</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Chateau St. Jean Fume Blanc, Sonoma

</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Elk Cove Pinot Gris, Willamette Valley, OR, USA

</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Chateaus St. Michelle Riesling, Washington, USA

</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Rodney Strong Chardonnay, Sonoma

</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Schug Chardonnay, Cameros

</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    </br>
                    </br>

                    {{--Beer start--}}

                    <h2>Beer</h2>
                    <ul class="saffron_menu2_2_block_item">
                        <li class="saffron_prod_list_cont">
                            <a href="#">
                                <h3>Indian</h3>
                                <h5 class="saffron_prod_list_title"><span>KingFisher</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Flying Horse Lager</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Taj Mahal Lager</span></h5>

                                <div class="saffron_prod_list_price"></div>
                            </a>
                        </li>
                        <li class="saffron_prod_list_cont">
                            <a href="#">
                                <h3>Imported</h3>
                                <h5 class="saffron_prod_list_title"><span>Newcastle Brown Ale</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Guinness Draft Bottle</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Corona Light</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Heineken</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Kaliber - Non Alcoholic</span></h5>

                                <div class="saffron_prod_list_price"></div>
                            </a>
                        </li>
                        <li class="saffron_prod_list_cont">
                            <a href="#">
                                <h3>Domestic</h3>
                                <h5 class="saffron_prod_list_title"><span>Bud Light</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Budweiser</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Coors Light</span></h5>
                                </br>
                                <h5 class="saffron_prod_list_title"><span>Blue Moon</span></h5>

                                <div class="saffron_prod_list_price"></div>
                            </a>
                        </li>

                    </ul>
                </ul>
            </div>
            <div class="col col-6">
                <h2>Red Wine</h2>
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Miguel Torres Merlot, Chile</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Brancott Pinot Noir, New Zealand
</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Black Opal Shiraz, Australia
</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Red Diamond Cabernet Sauv, WA, USA

</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Bogle Zinfandel, California, USA

</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Greg Norman shiraz, Australia

</span></h5>
                            <div class="saffron_prod_list_price"></div>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>clos Du Bois Meriot, Sonoma

</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Kings Ridge Pinot noir, Oregon, USA

</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Francis Coppola Pinot Noir, Napa

</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>Franciscan Cabernet Sauvignon, Napa

</span></h5>
                            <div class="saffron_prod_list_price"></div>

                        </a>
                    </li>
                    </br>
                    </br>

                    {{--Champagne--}}
                    <h2>Champagne</h2>
                    <ul class="saffron_menu2_2_block_item">
                        <li class="saffron_prod_list_cont">
                            <a href="#">
                                <h5 class="saffron_prod_list_title"><span>Madonna Kabinett Riesling, Genmany
</span></h5>
                                <div class="saffron_prod_list_price"></div>

                            </a>
                        </li>
                        <li class="saffron_prod_list_cont">
                            <a href="#">
                                <h5 class="saffron_prod_list_title"><span>Black Opal Chardonnay, Australia
</span></h5>
                                <div class="saffron_prod_list_price"></div>

                            </a>
                        </li>
                    </ul>

                </ul>
            </div>
        </div>
    </div>
    <div >
        <div class="row">
            <div class="col col-12 saffron_content">
                <div class="saffron_container saffron_team">
                    <div class="row gutters">
                        <div class="col col-4">
                            <div class="saffron_team_item">
                                <div class="saffron_team_image">
                                    <img src="img/team_1-1600x1600.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col col-4">
                            <div class="saffron_team_item">
                                <div class="saffron_team_image">
                                    <img src="img/team_2-1600x1600.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col col-4">
                            <div class="saffron_team_item">
                                <div class="saffron_team_image">
                                    <img src="img/team_3-1600x1600.jpg" alt="">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

