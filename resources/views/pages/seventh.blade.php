@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Rice Entrees</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>

    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>VEGETABLE BIRYANI
</span></h5>
                            <div class="saffron_prod_list_price">$11.95</div>
                            <p class="saffron_prod_list_text">
                                Naturally fragrant basmati rice steam cooked with fresh gardern vegetables & exotic spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHICKEN BIRYANI
</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Chicken marinated with spices and saffron, then cooked with basmati rice.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>EGG BIRYANI
</span></h5>
                            <div class="saffron_prod_list_price">$12.95</div>
                            <p class="saffron_prod_list_text">Egg cooked with basmati rice, onions, peppers, herbs & spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>LAMB BIRYANI
</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">A classic moghul dish! Juicy lean pieces of lamb, cooked with basmati rice & spices.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SHRIMP BIRYANI
</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">Long grain basmati rice & jumbo shrimp cooked with blend of spices.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>GOAT BIRYANI
</span></h5>
                            <div class="saffron_prod_list_price">$14.95</div>
                            <p class="saffron_prod_list_text">Fresh goat (with bone) cooked with basmati rice, herbs & spices, cilantro.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>PATIALA PILLA
</span></h5>
                            <div class="saffron_prod_list_price">$7.95</div>
                            <p class="saffron_prod_list_text">Flavored basmati rice cooked with green peas, cumin, raisins & nuts.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SIDE ORDDER OF BASMATI RICE
</span></h5>
                            <div class="saffron_prod_list_price">$2.50</div>

                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_1-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_2-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_3-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

@endsection
