@extends('layouts.app')

@section('content')

    <title>Lunch Menu saffron</title>

    <div class="clear"></div>
    <div class="mobile_header ">
        <a href="/" class="saffron_image_logo"></a>
        <a href="javascript:void(0)" class="btn_mobile_menu">
            <span class="saffron_menu_line1"></span>
            <span class="saffron_menu_line2"></span>
            <span class="saffron_menu_line3"></span>
        </a>
    </div>
    <div class="col col-3 saffron_header_right">
        <div class="saffron_inner">

        </div>
    </div>

    <div class="saffron_title_block saffron_corners">
        <div class="saffron_inner_text">
            <h1>Beverages</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="saffron_button" href="/menulist">Soups & Salads</a>
            <a class="saffron_button" href="/appetizers">Appetizers</a>
            <a class="saffron_button" href="/third">Tandoori</a>
            <a class="saffron_button" href="/fourth">Seafood</a>
            <a class="saffron_button"  href="/fifth">Chicken</a>
            <a class="saffron_button" href="/sixth">Vegan & Vegetarian</a>
            <a class="saffron_button" href="/seventh">Rice Entrees</a>
            <a class="saffron_button" href="/eighth">Breads</a>
            <a class="saffron_button" href="/ninth">Deserts & Side Orders</a>
            <a class="saffron_button" href="/tenth">Beverages</a>

        </div>
    </div>
    </br>


    <div class="saffron_menu2_2_block">
        <div class="row gutters">
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>LASSI SWEET OR MANGO OR STRAWBERRY
</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">
                                A delicious soothing yougurt shaken drink served sweet, mango or strawberry.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>FRUIT JUICES
</span></h5>
                            <div class="saffron_prod_list_price">$2.95</div>
                            <p class="saffron_prod_list_text">Mango, Orange, Cranberry, Apple or Pineapple.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>MINERAL WATER</span></h5>
                            <div class="saffron_prod_list_price">$8.95</div>
                            <p class="saffron_prod_list_text">Choice of Palak Panir OR Veg Korma OR chana masala served with rice and naan bread.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SODAS
</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>
                            <p class="saffron_prod_list_text">Pepsi, Diet Pepsi, Sierra Mist, Dr. Pepper, Lemonade & others.</p>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>ICED TEA (Sweet or Unsweetened)</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>COFFEE (Freshly Brewed)</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>

                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-6">
                <ul class="saffron_menu2_2_block_item">
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>INDIAN TEA (Tea brewed with cardamoms)</span></h5>
                            <div class="saffron_prod_list_price">$1.95</div>

                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>SPICE TEA
</span></h5>
                            <div class="saffron_prod_list_price">$2.45</div>
                            <p class="saffron_prod_list_text">Sweet or unsweet, cold Indian Spice Tea.</p>
                        </a>
                    </li>
                    <li class="saffron_prod_list_cont">
                        <a href="#">
                            <h5 class="saffron_prod_list_title"><span>CHAI
</span></h5>
                            <div class="saffron_prod_list_price">$2.45</div>
                            <p class="saffron_prod_list_text">Darjeeling tea made with flavorful spices and milk then boiled together in water.</p>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
            <div >
                <div class="row">
                    <div class="col col-12 saffron_content">
                        <div class="saffron_container saffron_team">
                            <div class="row gutters">
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_1-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_2-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="saffron_team_item">
                                        <div class="saffron_team_image">
                                            <img src="img/team_3-1600x1600.jpg" alt="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

@endsection
