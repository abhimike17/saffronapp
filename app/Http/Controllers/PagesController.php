<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        return view('pages.index');
    }

    public function about(){
        return view('pages.about');
    }
    public function menu(){
        return view('pages.menu');
    }
    public function contact(){
        return view('pages.contact');
    }
    public function product(){
        return view('pages.product');
    }
    public function menulist(){
        return view('pages.menulist');
    }
    public function libations(){
        return view('pages.libations');
    }
    public function appetizers(){
        return view('pages.appetizers');
    }
    public function third(){
        return view('pages.third');
    }
    public function bar(){
        return view('pages.bar');
    }
    public function fourth(){
        return view('pages.fourth');
    }
    public function fifth(){
        return view('pages.fifth');
    }
    public function sixth(){
        return view('pages.sixth');
    }
    public function seventh(){
        return view('pages.seventh');
    }
    public function eighth(){
        return view('pages.eighth');
    }
    public function ninth(){
        return view('pages.ninth');
    }
    public function tenth(){
        return view('pages.tenth');
    }
}
